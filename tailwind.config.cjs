/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './index.html',
    './src/**/*.{ts,tsx,js,jsx}'
  ],
  important: '#root',
  theme: {
    extend: {},
  },
  plugins: [],
}

