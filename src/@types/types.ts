import { Participant, Group, Round, Match, Stage } from 'brackets-model';
import standardData from '../assets/data/standardChips.json';
import megaData from '../assets/data/megaChips.json';
import gigaData from '../assets/data/gigaChips.json';

import standardImages from '../assets/data/bn_6_chip_images/standardLibrary';
import megaImages from '../assets/data/bn_6_chip_images/megaLibrary';
import gigaImages from '../assets/data/bn_6_chip_images/gigaLibrary';
import { placeholderChip } from '../assets/data/bn_6_chip_images';


export type RegistrationMetadata = string

export interface SimpleChipData {
    id: string,
    code: string,
    duplicates?: number
}

export interface BattleNetworkMeta {
    folders?: SimpleChipData[][]
}

export enum RegistrationSearchType {
    uuid = 'uuid',
    displayName = 'displayName',
    discordName = 'discordName'
}

export interface Registration {
    uuid: string,
    displayName: string,
    discordName?: string,
    game?: string,
    metadata?: RegistrationMetadata
}

export interface Settings {
    size:              number;
    seedOrdering:      string[];
    grandFinal:        string;
    matchesChildCount: number;
}

type _ChipData = {
  id: string,
  name: string,
  codes: string[],
  mb: number,
  atk: number,
  element: string,
  description: string
}

export type ChipData = _ChipData & {
  img?: string,
  duplicates?: number
} 

export interface ChipLibrary {
    standard: ChipData[];
    mega: ChipData[];
    giga: ChipData[];
}

export const placeholderChipImage = () => new URL(placeholderChip, import.meta.url).href;

const dataToChipWithImage = (dataLibrary: _ChipData[], imageLibrary: string[]) => {
    return dataLibrary.map((chip) => {
        let targetId = '';
        let imageSrc = undefined;

        if (chip.id.length === 3) {
            targetId = chip.id;
            imageSrc = imageLibrary.find((image) => image.includes(targetId));
        } else {
            targetId = chip.id.slice(2);
            const prefix = chip.id.slice(0,2);
            imageSrc = imageLibrary.find((image) => image.includes(prefix) && image.includes(targetId));
        }

        const resolvedUrl = imageSrc
            ? new URL(imageSrc, import.meta.url).href
            : undefined;

        return {
            ...chip,
            img: resolvedUrl
        };
    });

}

export const buildLibrary = (): ChipLibrary => {
    const standardLibrary = dataToChipWithImage(standardData, standardImages);
    const megaLibrary = dataToChipWithImage(megaData, megaImages);
    const gigaLibrary = dataToChipWithImage(gigaData, gigaImages);

    return {
        standard: standardLibrary,
        mega: megaLibrary,
        giga: gigaLibrary
    }
}

export type BattleNetworkParticipant = Participant & {
    folder?: ChipData[];
    version?: string;
    navicust: 'https://i.imgur.com/BH32Qz8.png';
};

type  ParticipantListType =  Participant[] | BattleNetworkParticipant[];

export interface BracketDataModel {
    participant: ParticipantListType;
    stage:       Stage[];
    group:       Group[];
    round:       Round[];
    match:       Match[];
    match_game:  any[];
}