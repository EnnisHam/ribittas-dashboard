import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import { StyledEngineProvider, ThemeProvider, createTheme } from '@mui/material/styles'
import 'tailwindcss/tailwind.css'
import { RouterProvider } from '@tanstack/react-router'
import router from './routes.tsx'

const darkTheme = createTheme({
  palette: {
    mode: 'dark'
  }
})

declare module '@tanstack/react-router' {
  interface Register {
    // This infers the type of our router and registers it across your entire project
    router: typeof router
  }
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={darkTheme}>
        <RouterProvider router={router}/>
      </ThemeProvider>
    </StyledEngineProvider>
  </React.StrictMode>,
);
