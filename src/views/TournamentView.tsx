import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { BracketsManager, Database } from 'brackets-manager'; 
import { Match, MatchGame, Participant, Stage } from 'brackets-model';
import { InMemoryDatabase } from 'brackets-memory-db';

import '../App.css'
import { BracketViewer } from '../components/bracket-view';
import sample from '../assets/sampleBracket.json';
import { BracketDataModel } from '../@types/types';
// import RibbitaImage from './assets/Ribbita_2.webp';
import { SideNavigation } from '../components/navigation/sidebar';
import { IMatchComposition } from '../components/navigation/match';
import { MatchModal } from '../components/match/modal/matchInfo';
import { DeckDrawer } from '../components/deck/drawer/drawer';
import { Typography } from '@mui/material';

const TournamentView = () => {
  const dataStorage = useMemo(() => {
    const memoryStorage = new InMemoryDatabase();
    memoryStorage.setData(sample as Database);
    return memoryStorage;
  }, [sample]);

  const manager = useRef(new BracketsManager(dataStorage));
  const [dataModel, setDataModel] = useState<BracketDataModel>();

  const [matchDetails, setMatchDetails] = useState<IMatchComposition | undefined>(undefined);

  // initialize data
  useEffect(() => {
    const initializeManager = async () => {
      const data = await manager.current.get.stageData(0)
      setDataModel(data);
    }
    initializeManager();
  }, [manager]);

  const updateDataModel = useCallback(async () => {
    const dataManager = manager.current;
    const newData = await dataManager.get.stageData(0);
    setDataModel(newData);
  }, [manager]);

  const updateMatch = useCallback(async (input: IMatchComposition) => {
    await manager.current.update.match(input);
    await updateDataModel();
  }, []);

  const autoBye = async () => {
    await manager.current.update.confirmSeeding(0);
    await updateDataModel();
  };

  const [modalState, setModalState] = useState(false);
  const openModal = () => setModalState(true);
  const closeModal = () => setModalState(false);

  const [drawerState, setDrawerState] = useState(false);
  const openDrawer = () => setDrawerState(true);
  const closeDrawer = () => setDrawerState(false);

  return dataModel && (
    <div>
      <h1 className='text-6xl font-semibold'> Bracket dataModel</h1>
      <div className='flex flex-row'>
        <SideNavigation
          groupInfo={dataModel?.group ?? []}
          roundInfo={dataModel?.round ?? []}
          matchInfo={dataModel?.match ?? []}
          participantInfo={dataModel?.participant ?? []}
          openModal={openModal}
          setMatchDetails={setMatchDetails}
          className='mr-4'
        />
        <BracketViewer
          data={dataModel ?? {
            participant: [] as Participant[],
            stage: [] as Stage[],
            match: [] as Match[],
            match_game: [] as MatchGame[]
          } as BracketDataModel}
          openModal={openModal}
          setMatchDetails={setMatchDetails}
          style={{
            flexGrow: 1
          }}
        />
        <MatchModal
          details={matchDetails}
          open={modalState}
          onClose={closeModal}
          updateMatch={updateMatch}
          autoBye={autoBye}
          openDrawer={openDrawer}
        />
        <DeckDrawer drawerState={drawerState} onClose={closeDrawer} />

        {/* <picture>
          <img
            src={RibbitaImage}
            style={{
              position: 'absolute',
              left: '1200px',
              width: 600,
              height: 'auto'
            }}
          />
        </picture> */}
      </div>
    </div>
  )
}

export default TournamentView
