import { useMemo, useState } from 'react';
import {
  ChipData,
  ChipLibrary,
  buildLibrary,
  placeholderChipImage
} from '../@types/types';
import '../App.css'
// import { useSearch } from '@tanstack/react-router';
import { LibraryCategoryDisplay, PackagedSetDisplay } from '../components/deck/folder';
import { chunk, cloneDeep } from 'lodash';
import { transpose } from '../utils/matrix';
import { DefaultPackageChip } from '../components/deck/chip';

// interface FolderBuilderQueryParams {
//   user?: string
// }

// const mockedRegistration = {
//   uuid: 'foooasdfzxcvuwe',
//   displayName: 'Ennis Ham',
//   discordName: 'ennis'
// }


const getLibrary = (key: string = '', library: ChipLibrary) => {
  if (key.toLowerCase() === 'giga') {
    return library.giga
  }

  if (key.toLowerCase() === 'mega') {
    return library.mega
  }

  return library.standard
}


interface DeckTrayProps {
  chips: ChipData[]
}

const DeckTray = (props: DeckTrayProps) => {
  const { chips } = props;

  const chipChunks = chunk(chips, 6);
  const dataTransposed = transpose(chipChunks);

  const displaySet = dataTransposed.map(
    (chips, idx) =>
      <PackagedSetDisplay
        key={`deck-tray-${idx}`}
        set={chips}
        identifier={`deck-tray-${idx}`}
        minimal
      />
  );

  return (
    <div>
      Deck Builder Tray
      <div className='flex flex-row gap-0'>
        {displaySet}
      </div>
    </div>
  )
}

interface DeckSummaryProps {
  chips: ChipData[]
}

const DeckSummary = (props: DeckSummaryProps) => {
  const chips = props.chips.filter((chip) => chip.id !== 'PLACE HOLDER');
  return (
    <div>
      <span>Total Chips: {chips.length}</span>
      <ul>
        {chips.map((chip) => <li>{chip.name}</li>)}
      </ul>
    </div>
  )
}

const useDeckBuilder = (library: ChipLibrary) => {
  const placeholder = {
    id: 'PLACE HOLDER',
    name: 'PLACE HOLDER',
    codes: ['*'],
    mb: 0,
    atk: 0,
    element: 'Null',
    description: 'PLACE HOLDER',
    img: placeholderChipImage()
  }
  const [deck, setDeck] = useState<ChipData[]>(Array(30).fill(placeholder))

  const addToDeck = (id: string, code: string, libraryKey: string) => {
    const exists = deck.find((chip) => chip.id === id);
    if (exists) {
      return;
    }

    const chipLibrary = getLibrary(libraryKey, library);
    const chip = chipLibrary.find((entry) => entry.id === id);

    if (!chip) {
      return;
    }
    const chipCode = chip.codes.includes(code) ? code : chip.codes[0];

    const output = {
      ...chip,
      code: chipCode
    };

    setDeck((prev) => {
      const unique = prev.filter((chip) => chip.id !== 'PLACE HOLDER');
      const rest = prev.filter((chip) => chip.id === 'PLACE HOLDER');
      rest.pop()
      return [output, ...unique, ...rest]
    });
  }

  const removeFromDeck = (id: string) => {
    setDeck((prev) => {
      const copy = prev.slice();
      const targetIndex = copy.findIndex((chip) => chip.id === id);
      copy[targetIndex] = cloneDeep(placeholder) // impure function :(

      return copy
    });
  }


  return [deck, addToDeck, removeFromDeck] as [ChipData[], (id: string, code: string, libraryKey: string) => void, (id: string) => void]
}

const FolderBuilderView = () => {
  // const { user } = useSearch() as FolderBuilderQueryParams
  // const player = mockedRegistration
  const library = useMemo(() => buildLibrary(), []);
  const [deck, addToDeck, removeFromDeck] = useDeckBuilder(library);

  return (
    <div>
      <div className='my-8 justify-center'>
        Folder Builder
      </div>
      <div className='flex gap-5'>
        <div className='w-1/2 flex flex-col gap-16'>
          <DeckSummary chips={deck} />
          <DeckTray chips={deck} />
        </div>
        <div className='flex-1'>
          <LibraryCategoryDisplay
            className='bg-orange-600 rounded-t-lg'
            chips={library.giga}
            label="Giga"
            addToDeck={(_id, _code) => addToDeck(_id, _code, 'giga')}
            removeFromDeck={removeFromDeck}
          />
          <LibraryCategoryDisplay
            className='bg-blue-600'
            chips={library.mega}
            label="Mega"
            addToDeck={(_id, _code) => addToDeck(_id, _code, 'mega')}
            removeFromDeck={removeFromDeck}
          />
          <LibraryCategoryDisplay
            className='bg-slate-600'
            chips={library.standard}
            label="Standard"
            addToDeck={(_id, _code) => addToDeck(_id, _code, 'standard')}
            removeFromDeck={removeFromDeck}
          />
        </div>
      </div>
    </div>
  )
}

export default FolderBuilderView;