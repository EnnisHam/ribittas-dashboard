import { Link, Outlet, RootRoute, Route, Router } from "@tanstack/react-router"
import { QueryClient, QueryClientProvider } from "@tanstack/react-query"
import TournamentView from "./views/TournamentView"
import FolderBuilderView from "./views/FolderBuilderView"

export const placeholder = () => (<div>No Content</div>)

interface FolderBuilderQueryParams {
  user?: string
}

const createRoutes = () => {
  const queryClient = new QueryClient()
  const DefaultRoute = new RootRoute({
    component: () => {
      return (
        <QueryClientProvider client={queryClient}>
          <div>
            <div
              className="flex justify-evenly text-red-500 text-2xl"
            >
              <Link to="/tournaments">Tournament</Link>
              <Link to="/folder-builder" search={{user: undefined}}>Folder Builder</Link>
            </div>
            <hr/>
            <Outlet />
          </div>
        </QueryClientProvider>
      )
    }
  })

  const TournamentRoute = new Route({
    getParentRoute: () => DefaultRoute,
    path: '/tournaments',
    component: TournamentView
  })
  const FolderBuilderRoute = new Route({
    getParentRoute: () => DefaultRoute,
    path: '/folder-builder',
    component: FolderBuilderView,
    validateSearch: (search?: Record<string, unknown>): FolderBuilderQueryParams => {
      return {
        user: search?.user as string || ''
      }
    },
    loader: ({ search }) => {
      search
    }
  })

  const routeTree = DefaultRoute.addChildren([TournamentRoute, FolderBuilderRoute])
  const router = new Router({ routeTree })
  return router
}

const router = createRoutes()
export default router
