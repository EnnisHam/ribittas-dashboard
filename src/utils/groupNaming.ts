export enum TournamentType {
    roundRobin = 1,
    singleElimination = 2,
    doubleElimination = 3
}

export const numberToTournamentType = (input: number) => {
    switch(input) {
        case 1: return TournamentType.roundRobin;
        case 2: return TournamentType.singleElimination;
        case 3: return TournamentType.doubleElimination;
        default:
            console.error(`invalid input for numberToTournamentType ${input}`);
            break;
    }
};

export const createGroupLabel = (groupId: number, stage: TournamentType) => {
    if (![0,1,2].includes(groupId)) {
        console.error(`group id ${groupId} is not valid. values 0 to 2 are supported`);
        return;
    }

    switch(stage) {
        case TournamentType.roundRobin:
            return 'Pool';
        case TournamentType.singleElimination:
            return groupId === 0 ? 'Bracket' : 'Consolidation';
        case TournamentType.doubleElimination:
            if (groupId === 0) return 'Winners Bracket';
            if (groupId === 1) return 'Losers Bracket';
            if (groupId === 2) return 'Grand Finals';
            console.error('unknown group type');
            return '';
        default:
            console.error('unknown tournament type');
            return '';
    }
}