import { ChipData } from '../@types/types';

export const transpose = (matrix: ChipData[][]) => {
  if (matrix.length === 0) {
    return matrix;
  }
  let maxRow = matrix.length;
  const columnLengths = matrix.map((row) => row.length);
  let maxCol = Math.max(...columnLengths);

  let output = new Array<ChipData[]>(maxCol);

  for (let rowIndex = 0; rowIndex < maxRow; rowIndex++) {
    output[rowIndex] = new Array<ChipData>(maxCol);

    for (let columnIndex = 0; columnIndex < maxCol; columnIndex++) {
      output[rowIndex][columnIndex] = matrix[rowIndex][columnIndex];
    }
  }

  return output[0]?.map(
    (_, columnIndex) => output?.map((row) => row[columnIndex] ?? undefined)
  );
}