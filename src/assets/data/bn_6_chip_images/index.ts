import Bn6Library from './standardLibrary';
import Bn6MegaLibrary from './megaLibrary';
import Bn6GigaLibrary from './gigaLibrary';

const placeholderChip = '../assets/data/bn_6_chip_images/chip_placeholder.png';

export { placeholderChip, Bn6Library, Bn6MegaLibrary, Bn6GigaLibrary };