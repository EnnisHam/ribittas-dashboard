const BN6MChipPatch = '../assets/data/bn_6_chip_images/BN6MChipPatch.png';
const BN6MChip001 = '../assets/data/bn_6_chip_images/BN6MChip001.png';
const BN6MChip002 = '../assets/data/bn_6_chip_images/BN6MChip002.png';
const BN6MChip003 = '../assets/data/bn_6_chip_images/BN6MChip003.png';
const BN6MChip004 = '../assets/data/bn_6_chip_images/BN6MChip004.png';
const BN6MChip005 = '../assets/data/bn_6_chip_images/BN6MChip005.png';
const BN6MChip006 = '../assets/data/bn_6_chip_images/BN6MChip006.png';
const BN6MChip022 = '../assets/data/bn_6_chip_images/BN6MChip022.png';
const BN6MChip023 = '../assets/data/bn_6_chip_images/BN6MChip023.png';
const BN6MChip024 = '../assets/data/bn_6_chip_images/BN6MChip024.png';
const BN6MChip025 = '../assets/data/bn_6_chip_images/BN6MChip025.png';
const BN6MChip026 = '../assets/data/bn_6_chip_images/BN6MChip026.png';
const BN6MChip027 = '../assets/data/bn_6_chip_images/BN6MChip027.png';
const BN6MChip028 = '../assets/data/bn_6_chip_images/BN6MChip028.png';
const BN6MChip029 = '../assets/data/bn_6_chip_images/BN6MChip029.png';
const BN6MChip030 = '../assets/data/bn_6_chip_images/BN6MChip030.png';
const BN6MChip031 = '../assets/data/bn_6_chip_images/BN6MChip031.png';
const BN6MChip032 = '../assets/data/bn_6_chip_images/BN6MChip032.png';
const BN6MChip033 = '../assets/data/bn_6_chip_images/BN6MChip033.png';
const BN6MChip034 = '../assets/data/bn_6_chip_images/BN6MChip034.png';
const BN6MChip035 = '../assets/data/bn_6_chip_images/BN6MChip035.png';
const BN6MChip036 = '../assets/data/bn_6_chip_images/BN6MChip036.png';
const BN6MChip037 = '../assets/data/bn_6_chip_images/BN6MChip037.png';
const BN6MChip038 = '../assets/data/bn_6_chip_images/BN6MChip038.png';
const BN6MChip039 = '../assets/data/bn_6_chip_images/BN6MChip039.png';
const BN6MChip040 = '../assets/data/bn_6_chip_images/BN6MChip040.png';
const BN6MChip041 = '../assets/data/bn_6_chip_images/BN6MChip041.png';
const BN6MChip042 = '../assets/data/bn_6_chip_images/BN6MChip042.png';
const BN6MChip043 = '../assets/data/bn_6_chip_images/BN6MChip043.png';
const BN6MChip044 = '../assets/data/bn_6_chip_images/BN6MChip044.png';
const BN6MChip045 = '../assets/data/bn_6_chip_images/BN6MChip045.png';

const BN6FMChip007 = '../assets/data/bn_6_chip_images/BN6FMChip007.png';
const BN6FMChip008 = '../assets/data/bn_6_chip_images/BN6FMChip008.png';
const BN6FMChip009 = '../assets/data/bn_6_chip_images/BN6FMChip009.png';
const BN6FMChip010 = '../assets/data/bn_6_chip_images/BN6FMChip010.png';
const BN6FMChip011 = '../assets/data/bn_6_chip_images/BN6FMChip011.png';
const BN6FMChip012 = '../assets/data/bn_6_chip_images/BN6FMChip012.png';
const BN6FMChip013 = '../assets/data/bn_6_chip_images/BN6FMChip013.png';
const BN6FMChip014 = '../assets/data/bn_6_chip_images/BN6FMChip014.png';
const BN6FMChip015 = '../assets/data/bn_6_chip_images/BN6FMChip015.png';
const BN6FMChip016 = '../assets/data/bn_6_chip_images/BN6FMChip016.png';
const BN6FMChip017 = '../assets/data/bn_6_chip_images/BN6FMChip017.png';
const BN6FMChip018 = '../assets/data/bn_6_chip_images/BN6FMChip018.png';
const BN6FMChip019 = '../assets/data/bn_6_chip_images/BN6FMChip019.png';
const BN6FMChip020 = '../assets/data/bn_6_chip_images/BN6FMChip020.png';
const BN6FMChip021 = '../assets/data/bn_6_chip_images/BN6FMChip021.png';
const BN6FMChip022 = '../assets/data/bn_6_chip_images/BN6FMChip022.png';
const BN6FMChip023 = '../assets/data/bn_6_chip_images/BN6FMChip023.png';
const BN6FMChip024 = '../assets/data/bn_6_chip_images/BN6FMChip024.png';
const BN6FMChip025 = '../assets/data/bn_6_chip_images/BN6FMChip025.png';
const BN6FMChip026 = '../assets/data/bn_6_chip_images/BN6FMChip026.png';
const BN6FMChip027 = '../assets/data/bn_6_chip_images/BN6FMChip027.png';

const BN6GMChip007 = '../assets/data/bn_6_chip_images/BN6GMChip007.png';
const BN6GMChip008 = '../assets/data/bn_6_chip_images/BN6GMChip008.png';
const BN6GMChip009 = '../assets/data/bn_6_chip_images/BN6GMChip009.png';
const BN6GMChip010 = '../assets/data/bn_6_chip_images/BN6GMChip010.png';
const BN6GMChip011 = '../assets/data/bn_6_chip_images/BN6GMChip011.png';
const BN6GMChip012 = '../assets/data/bn_6_chip_images/BN6GMChip012.png';
const BN6GMChip013 = '../assets/data/bn_6_chip_images/BN6GMChip013.png';
const BN6GMChip014 = '../assets/data/bn_6_chip_images/BN6GMChip014.png';
const BN6GMChip015 = '../assets/data/bn_6_chip_images/BN6GMChip015.png';
const BN6GMChip016 = '../assets/data/bn_6_chip_images/BN6GMChip016.png';
const BN6GMChip017 = '../assets/data/bn_6_chip_images/BN6GMChip017.png';
const BN6GMChip018 = '../assets/data/bn_6_chip_images/BN6GMChip018.png';
const BN6GMChip019 = '../assets/data/bn_6_chip_images/BN6GMChip019.png';
const BN6GMChip020 = '../assets/data/bn_6_chip_images/BN6GMChip020.png';
const BN6GMChip021 = '../assets/data/bn_6_chip_images/BN6GMChip021.png';
const BN6GMChip022 = '../assets/data/bn_6_chip_images/BN6GMChip022.png';
const BN6GMChip023 = '../assets/data/bn_6_chip_images/BN6GMChip023.png';
const BN6GMChip024 = '../assets/data/bn_6_chip_images/BN6GMChip024.png';
const BN6GMChip025 = '../assets/data/bn_6_chip_images/BN6GMChip025.png';
const BN6GMChip026 = '../assets/data/bn_6_chip_images/BN6GMChip026.png';
const BN6GMChip027 = '../assets/data/bn_6_chip_images/BN6GMChip027.png';

export default [
  BN6MChipPatch,
  BN6MChip001,
  BN6MChip002,
  BN6MChip003,
  BN6MChip004,
  BN6MChip005,
  BN6MChip006,
  BN6MChip022,
  BN6MChip023,
  BN6MChip024,
  BN6MChip025,
  BN6MChip026,
  BN6MChip027,
  BN6MChip028,
  BN6MChip029,
  BN6MChip030,
  BN6MChip031,
  BN6MChip032,
  BN6MChip033,
  BN6MChip034,
  BN6MChip035,
  BN6MChip036,
  BN6MChip037,
  BN6MChip038,
  BN6MChip039,
  BN6MChip040,
  BN6MChip041,
  BN6MChip042,
  BN6MChip043,
  BN6MChip044,
  BN6MChip045,
  BN6FMChip007,
  BN6FMChip008,
  BN6FMChip009,
  BN6FMChip010,
  BN6FMChip011,
  BN6FMChip012,
  BN6FMChip013,
  BN6FMChip014,
  BN6FMChip015,
  BN6FMChip016,
  BN6FMChip017,
  BN6FMChip018,
  BN6FMChip019,
  BN6FMChip020,
  BN6FMChip021,
  BN6FMChip022,
  BN6FMChip023,
  BN6FMChip024,
  BN6FMChip025,
  BN6FMChip026,
  BN6FMChip027,

  BN6GMChip007,
  BN6GMChip008,
  BN6GMChip009,
  BN6GMChip010,
  BN6GMChip011,
  BN6GMChip012,
  BN6GMChip013,
  BN6GMChip014,
  BN6GMChip015,
  BN6GMChip016,
  BN6GMChip017,
  BN6GMChip018,
  BN6GMChip019,
  BN6GMChip020,
  BN6GMChip021,
  BN6GMChip022,
  BN6GMChip023,
  BN6GMChip024,
  BN6GMChip025,
  BN6GMChip026,
  BN6GMChip027,
];