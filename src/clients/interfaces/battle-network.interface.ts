import { BattleNetworkMeta, Registration, RegistrationMetadata, RegistrationSearchType } from "../../@types/types";
import { DataBaseManager } from "../google-sheets-client";

export class BattleNetworkDatabase {
  private _database: DataBaseManager<Registration>;
  private static CacheInterval = 300000
  private readonly __cachingRef: number;

  constructor(tableName: string, credentials: { serviceEmail: string, privateKey: string }) {
    this._database = new DataBaseManager(tableName, credentials);
    this.__cachingRef = setInterval(
      this.cacheRoutine.bind(this),
      BattleNetworkDatabase.CacheInterval);
  }

  private async cacheRoutine() {
    const current = await this._database.getCurrentSheet()

    if (current) {
      this._database.cacheRows(current);
    }
  }

  public async getParticipant(input: string, key: RegistrationSearchType) {
    const currentRows = await this._database.getRows()
    if (!currentRows) {
      return
    }

    return currentRows.find((row) => row.get(key) === input)
  }

  public async getParticipantFolders(input: string, key: RegistrationSearchType) {
    const target = await this.getParticipant(input, key)
    if (!target) {
      return
    }

    const metadata: BattleNetworkMeta = JSON.parse(target.get('metadata') as RegistrationMetadata)

    return metadata
  }

}