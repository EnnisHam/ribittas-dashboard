import { GoogleSpreadsheet, GoogleSpreadsheetRow, GoogleSpreadsheetWorksheet } from 'google-spreadsheet';
import { JWT } from 'google-auth-library';
import { DateTime } from 'luxon';

export class DataBaseManager<T extends Record<string, any>> {
    private database: GoogleSpreadsheet;
    private currentSheet?: {
        sheet: GoogleSpreadsheetWorksheet,
        acquired: DateTime
    };
    private loadedRows?: GoogleSpreadsheetRow<T>[];
    private jwt: JWT;

    constructor(targetSheet: string, credentials: { serviceEmail: string, privateKey: string}) {
        this.jwt = new JWT({
            email: credentials.serviceEmail,
            key: credentials.privateKey
        });
        this.database = new GoogleSpreadsheet(targetSheet, this.jwt);
    }

    public async getCurrentSheet() {
        return this.currentSheet?.sheet;
    }

    public async authenticateAndLoad() {
        await this.database.loadInfo();
        console.log(`Loaded ${this.database.title}`);
    }

    public getSheet(sheetName: string) {
        this.currentSheet = {
            sheet: this.database.sheetsByTitle[sheetName],
            acquired: DateTime.now()
        }
        this.cacheRows(this.currentSheet.sheet);
        return this.currentSheet;
    }

    public getHeaderKeys() {
        const headers = this.currentSheet?.sheet.headerValues;
        if (!headers) {
            console.error('headerValues are undefined getRows() needs to be loaded first');
            return undefined;
        }
        return headers;
    }

    public async cacheRows(sheet: GoogleSpreadsheetWorksheet) {
        const rows = await sheet.getRows<T>();
        this.loadedRows = rows;
    }

    public async getRows(sheet?: string): Promise<GoogleSpreadsheetRow<T>[] | undefined> {

        // if the path to minutes is undefined then just refetch the rows;
        if ((this.currentSheet?.acquired?.diffNow().minutes ?? 4) > 4) {
            console.log(`refetching ${this.currentSheet?.sheet.title}`);
            const rows = await this.currentSheet?.sheet.getRows<T>();
            this.loadedRows = rows;
        }

        if (this.loadedRows) {
            return this.loadedRows;
        }

        if (sheet) {
            this.getSheet(sheet);
            return this.loadedRows;
        }

        // TODO: clean up logic flow
        return await this.currentSheet?.sheet.getRows();
    }

    public async addRow<T extends Record<string, any>>(rowData: T) {
        const row = await this.currentSheet?.sheet.addRow(rowData);
        if (row) {
            this.loadedRows?.push(row as any);
        }
        return row 
    }

}