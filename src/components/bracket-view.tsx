import { useCallback, useEffect, useMemo } from 'react';
import { BracketDataModel } from '../@types/types';
import { Match } from 'brackets-model';
import { IMatchComposition, composeMatchDetails } from './navigation/match';
import './bracket-view.css';

type PropTypes = {
  data: BracketDataModel;
  openModal: () => void;
  setMatchDetails: (details: IMatchComposition) => void;
  style?: React.CSSProperties;
};

export const BracketViewer = (props: PropTypes) => {
  const {
    openModal,
    setMatchDetails
  } = props;

  // memoize to force rerender on parent update?
  const managerData = useMemo(() => props.data, [props.data]);

  const matchClickInteraction = useCallback((match: Match) => {
    const matchDetails = composeMatchDetails(match, props.data.participant);
    setMatchDetails(matchDetails);
    openModal();
  }, [setMatchDetails, openModal]);

  const render = useCallback((data: BracketDataModel) => {
    // smh linter gaslighting you
    // @ts-ignore
    window.bracketsViewer.render({
      stages: data.stage,
      matches: data.match,
      matchGames: data.match_game,
      participants: data.participant,
    },
    {
      participantOriginPlacement: 'before',
      separatedChildCountLabel: true,
      showSlotsOrigin: true,
      showLowerBracketSlotsOrigin: true,
      highlightParticipantOnHover: true,
      onMatchClick: matchClickInteraction
    });
  }, [matchClickInteraction]);

  useEffect(() => {
    const previousRender = document.getElementsByClassName('elimination');
    if (previousRender) {
      for (let i = 0; i < previousRender.length; i++) {
        previousRender.item(i)?.remove();
      }
    }
    render(managerData);
  }, [managerData, matchClickInteraction]);

  return (
    <div className='brackets-viewer' style={{ ...props.style }}></div>
  );
}
