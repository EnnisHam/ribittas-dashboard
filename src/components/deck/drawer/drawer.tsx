import { Drawer, Button, styled, List } from '@mui/material';
import { useMemo } from 'react';
import { ChipData, buildLibrary } from '../../../@types/types';
import { FolderDisplay } from '../folder';

const StyledDrawer = styled(Drawer) ({
  '& .MuiBackdrop-root': {
    background: 'transparent'
  },
  '& .MuiDrawer-paper': {
    width: '30vw'
  }
})

type DeckDrawerProps = {
  drawerState: boolean,
  onClose: () => void,
  style?: React.CSSProperties
}

const pick = (source: ChipData[], target: string) => {
  return source.find((chip) => chip.id === target) as ChipData;
}

export const DeckDrawer = (props: DeckDrawerProps) => {
  const { drawerState, onClose } = props;
  const library = buildLibrary()

  const folder = useMemo(() => {
    const BblStar2 = pick(library.standard, '027');
    const DolThdr2 = pick(library.standard, "031");
    const ElcPuls3 = pick(library.standard, "035");
    const MachGun2 = pick(library.standard, "056");
    const FlshBom3 = pick(library.standard, "064");
    const WindRack = pick(library.standard, "079");
    const AreaGrab = pick(library.standard, "165");
    const GrabBnsh = pick(library.standard, "166");
    const BugFix = pick(library.standard, "178");
    const Invisibl = pick(library.standard, "179");
    const AntiDmg = pick(library.standard, "189");
    const WhiCapsl = pick(library.standard, "192");
    const Atk30 = pick(library.standard, "197");
    const SlashMnSP = pick(library.mega, 'GM015');
    const EraseMan = pick(library.mega, 'GM016');
    const JudgeMan = pick(library.mega, '031');
    const ColonelSp = pick(library.mega, '039');

    BblStar2.duplicates = 3;
    DolThdr2.duplicates = 3;
    ElcPuls3.duplicates = 2;
    MachGun2.duplicates = 2;
    WindRack.duplicates = 2;
    AreaGrab.duplicates = 3;
    Invisibl.duplicates = 2;
    AntiDmg.duplicates = 2;

    return [
      BblStar2,
      DolThdr2,
      ElcPuls3,
      MachGun2,
      FlshBom3,
      WindRack,
      AreaGrab,
      GrabBnsh,
      BugFix,
      Invisibl,
      AntiDmg,
      WhiCapsl,
      Atk30,
      SlashMnSP,
      EraseMan,
      JudgeMan,
      ColonelSp
    ].filter((chip) => chip) as ChipData[]
  }, []);

  return (
    <StyledDrawer
      anchor={'right'}
      open={drawerState}
      onClose={() => onClose()}
    >
      <Button onClick={() => onClose()}>Close</Button>
      <List>
        <FolderDisplay folder={folder}/>
      </List>
    </StyledDrawer>
  )
}