import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { ChipData } from "../../@types/types";
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  ListItem,
  Typography
} from '@mui/material';

export type ChipDisplayProps = Omit<ChipData, "codes"> & {
  code: string;
}
export const ChipDisplay = (props: ChipDisplayProps) => {
  const {
    id,
    name,
    code,
    mb,
    atk,
    element,
    description,
    img,
    duplicates
  } = props;

  return (
    <ListItem>
      <Card>
        <CardContent sx={{ width: '500px' }}>
          <div className='flex flex-row'>
            <img width={ 180 } height={ 210 } src={img}/>
            <div style={{ marginLeft: 12 }}>
              <Typography variant='h6'>ID: {id}</Typography>
              <Typography variant='h6'>Name: {name}</Typography>
              <Typography variant='h6'>Code: {code}</Typography>
              <Typography variant='h6'>MB: {mb}</Typography>
              <Typography variant='h6'>Atk: {atk}</Typography>
              <Typography variant='h6'>Element: {element}</Typography>
            </div>
            {duplicates && (
              <div className='ml-3'>
                <Typography variant='h3'>x{duplicates}</Typography>
              </div>
            )}
          </div>
          <div>
            <Typography variant='h6'>Description: {description}</Typography>
          </div>
        </CardContent>
      </Card>
    </ListItem>
  );
}

export const ChipPackageMinimal = (
  props: ChipData & {
    chipType?: string;
    addToDeck?: () => void;
    removeFromDeck?: () => void;
}) => {
  const {
    id,
    name,
    mb,
    atk,
    element,
    description,
    img,
    codes
  } = props;

  const handleEvent = (_: any, expanded: boolean) => {
    if (expanded === true) {
      if (props.removeFromDeck) {
        props.removeFromDeck()
      }
    }
  }

  return (
    <Accordion>
      <AccordionSummary
        className='h-32 m-1 bg-gray-600'
        id={`packaged-chip-display-${id}-${name}`}
      >
        <div className="flex flex-row">
          <img width={180} height={210} src={img}/>
        </div>
      </AccordionSummary>
      <AccordionDetails className={
        'flex flex-col bg-zinc-400 text-left flex-wrap'
      }>
        <Typography variant="h6">Name: {name}</Typography>
        <Typography variant="h6">Id: {id}</Typography>
        <Typography variant="h6">
          Codes: {codes.join(', ')}
        </Typography>
        <Typography variant="h6">MB: {mb}</Typography>
        <Typography variant="h6">Atk: {atk}</Typography>
        <Typography variant="h6">Element: {element}</Typography>
        <Typography variant="h6">Description: {description}</Typography>
      </AccordionDetails>
    </Accordion>
  )
}

export const ChipPackageDetailed = (
  props: ChipData & {
    chipType?: string;
    addToDeck?: () => void;
    removeFromDeck?: () => void;
}) => {
  const {
    id,
    name,
    mb,
    atk,
    element,
    description,
    img,
    codes
  } = props;

  const handleEvent = (_: any, expanded: boolean) => {
    if (expanded === true) {
      if (props.addToDeck) {
        props.addToDeck()
      }
    } else {
      if (props.removeFromDeck) {
        props.removeFromDeck()
      }
    }
  }

  const chipboarder = (() => {
    if (props.chipType?.toLowerCase() === 'giga') {
      return 'bg-yellow-600'
    }
    if (props.chipType?.toLowerCase() === 'mega') {
      return 'bg-blue-600'
    }
    return 'bg-slate-700'
  })()

  return (
    <Accordion onChange={handleEvent} className={chipboarder}>
      <AccordionSummary
        className='h-40 m-2 bg-gray-600'
        id={`packaged-chip-display-${id}-${name}`}
      >
        <div className="flex flex-row">
          <img width={180} height={210} src={img}/>
          <div className="block ml-3 text-left">
            <Typography variant="h6">Name: {name}</Typography>
            <Typography variant="h6">Id: {id}</Typography>
            <Typography variant="h6">
              Codes: {codes.join(', ')}
            </Typography>
          </div>
        </div>
      </AccordionSummary>
      <AccordionDetails className={
        'flex flex-col bg-zinc-400 text-left mt-2 flex-wrap'
      }>
        <Typography variant="h6">MB: {mb}</Typography>
        <Typography variant="h6">Atk: {atk}</Typography>
        <Typography variant="h6">Element: {element}</Typography>
        <Typography variant="h6">Description: {description}</Typography>
      </AccordionDetails>
    </Accordion>
  )
}

interface PackageDisplayProps extends ChipData {
  minimal?: boolean;
  chipType?: string;
  addToDeck?: () => void;
  removeFromDeck?: () => void;
}
export const ChipPackageDisplay = (props: PackageDisplayProps)=> {
  const { minimal } = props;
  return (
    minimal ? <ChipPackageMinimal {...props} /> : <ChipPackageDetailed {...props}/>
  )
}

export const DefaultPackageChip = () => {
  const props = {
    id: 'PLACE HOLDER',
    name: 'PLACE HOLDER',
    mb: 0,
    atk: 0,
    element: 'Null',
    description: 'PLACE HOLDER',
    img: '',
    codes: ['*']
  }
  return (
    <ChipPackageDisplay {...props}/>
  )
}