import { chunk } from 'lodash';
import { ChipData } from '../../@types/types';
import { ChipDisplay, ChipDisplayProps, ChipPackageDisplay } from './chip';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  List,
  Typography
} from '@mui/material';
import { transpose } from '../../utils/matrix';

type FolderProps = {
  folder: ChipData[]
}

export const FolderDisplay = (props: FolderProps) => {
  const { folder } = props;

  const folderDisplay = folder.map((chip, idx) => {
    const code = chip.codes[0]
    const props = {
      ...chip,
      code: code
    } as ChipDisplayProps;
    return <ChipDisplay key={`folder-${idx}`} {...props}/>
  });

  return (
    <List>
      {folderDisplay}
    </List>
  )
}

export interface setDisplayProps {
  set: ChipData[],
  identifier: string,
  minimal?: boolean,
  className?: string,
  chipType?: string,
  addToDeck?: (id: string, code: string) => void
  removeFromDeck?: (id: string) => void
}

export const PackagedSetDisplay = (props: setDisplayProps) => {
  const { set, identifier, minimal, addToDeck, removeFromDeck } = props;

  const setDisplay = set?.filter((chip) => !!chip)?.map(
    (chip, index) =>
      <ChipPackageDisplay
        key={`${identifier}-${index}`}
        minimal={minimal}
        {...chip}
        chipType={props.chipType}
        addToDeck={addToDeck ? () => addToDeck(chip.id, chip.codes[0]) : undefined}
        removeFromDeck={removeFromDeck ? () => removeFromDeck(chip.id) : undefined}
      />
  );

  const styles = `${props.className} flex flex-col gap-1`;

  return (
    <div
      className={styles}
      id={identifier}
    >
      {setDisplay}
    </div>
  )
}

export interface LibraryCategoryProps {
  chips: ChipData[]
  label: string
  className?: string
  addToDeck?: (id: string, code: string) => void
  removeFromDeck?: (id: string) => void
}

export const LibraryCategoryDisplay = (props: LibraryCategoryProps) => {
  const { chips, label, className } = props;
  const keyLabel = `${label ? `${label}-`: ''}set`

  const chipChunks = chunk(chips, 4);
  const dataTransposed = transpose(chipChunks);

  const displayChunks = dataTransposed?.map(
    (chipSet, idx) =>
      <PackagedSetDisplay
        set={chipSet}
        identifier={`${keyLabel}-${idx}`}
        className='w-96'
        chipType={label}
        addToDeck={props.addToDeck}
        removeFromDeck={props.removeFromDeck}
      />
  )

  return (
    <Accordion key={keyLabel} className={className}>
      <AccordionSummary>
        <Typography variant='h4'>{label}</Typography>
      </AccordionSummary>
      <AccordionDetails className='flex flex-row gap-1 overflow-scroll' sx={{
        maxHeight: '96rem !important'
      }}>
        {displayChunks}
      </AccordionDetails>
    </Accordion>
  )
}
