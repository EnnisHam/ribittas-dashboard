import { useMemo, useState } from 'react';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import { IMatchComposition } from '../../navigation/match';
import { styled } from '@mui/material/styles';
import { Alert, Autocomplete, TextField } from '@mui/material';
import { cloneDeep } from 'lodash-es';
import { Status } from 'brackets-model';
import { BattleNetworkParticipant, ChipData } from '../../../@types/types';

const PlayerLabel = styled('span')(({ theme }) => ({
  ...theme.typography.button,
  padding: theme.spacing(1)
}))

const StyledModal = styled(Modal)({
  '& .MuiBackdrop-root': {
    background: 'transparent'
  }
})

type PlayerEntryProps = {
  name?: string;
  folder?: ChipData[];
  version?: string;
  navicust?: string;
  openDrawer: () => void;
}

const PlayerEntry = (props: PlayerEntryProps) => {
  return (
    <div style={{
      display: 'flex',
      flexDirection: 'row'
    }}>
      <PlayerLabel>{props.name ?? 'Awaiting Player'}</PlayerLabel>
      <Button onClick={() => props.openDrawer()}>Folder</Button>
      {props.version && (
        <PlayerLabel>{props.version}</PlayerLabel>
      )}
    </div>
  )
}

const boxStyle = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '30vw',
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 5,
};

type ModalProps = {
  details: IMatchComposition | undefined;
  playerDetails?: { player1: BattleNetworkParticipant, player2: BattleNetworkParticipant }
  open: boolean;
  onClose: () => void;
  updateMatch: (match: IMatchComposition) => void;
  autoBye: () => void;
  openDrawer: () => void;
}

export const MatchModal = (props: ModalProps) => {
  const { details, playerDetails, updateMatch, autoBye, openDrawer } = props;

  const [newMatchWinner, setMatchWinner] = useState<string | undefined>()

  const player1 = useMemo(() =>
    <PlayerEntry
      name={details?.participants[0]?.name}
      folder={playerDetails?.player1.folder}
      navicust={playerDetails?.player1.navicust}
      version={playerDetails?.player1.version}
      openDrawer={openDrawer}
    />,
  [details?.participants[0]]);

  const player2 = useMemo(() =>
    <PlayerEntry
      name={details?.participants[1]?.name}
      folder={playerDetails?.player2.folder}
      navicust={playerDetails?.player2.navicust}
      version={playerDetails?.player2.version}
      openDrawer={openDrawer}
    />,
  [details?.participants[1]]);


  const autoCompleteOptions = useMemo(() => {
    if (!details) {
      return [] as string[]
    }
    return details.participants.map((player) => player?.name).filter((entry) => !!entry) as string[]
  }, [details]);

  const defaultMatchWinner = useMemo(() => {
    if (!details) {
      return undefined;
    }

    const opposingForces = [details.opponent1, details.opponent2];

    if (opposingForces.includes(null)) {
      return undefined;
    }

    const matchWinner = opposingForces.find((opponent) => opponent?.result === 'win');
    const participant = details.participants.find((player) => player?.id === matchWinner?.id);

    return participant?.name;
  }, [details, newMatchWinner]);

  const onUpdate = () => {
    if (!details) {
      return;
    }

    const winnerId = details.participants.find((player) => player?.name === newMatchWinner)?.id
    if (winnerId === undefined) {
      return;
    }
    const updatedCopy = cloneDeep(details)
    // resetting score because setting the status to completed will make a decision off of
    // points
    if (!updatedCopy.opponent1 || !updatedCopy.opponent2) {
      return;
    }
    updatedCopy.opponent1.score = 0;
    updatedCopy.opponent1.result = undefined;

    updatedCopy.opponent2.score = 0;
    updatedCopy.opponent2.result = undefined;

    if (updatedCopy.opponent1.id === winnerId) {
      updatedCopy.opponent1.score = 1
    }

    if (updatedCopy.opponent2.id === winnerId) {
      updatedCopy.opponent2.score = 1
    }


    updatedCopy.status = Status.Completed
    updateMatch(updatedCopy);
  }

  const notice = useMemo(() => {
    if (details?.status !== Status.Archived) {
      return null;
    }
    return <Alert severity='warning' sx={{ textAlign: 'center' }}>LOCKED</Alert>
  }, [details?.status]) 

  return (
    <StyledModal
      open={props.open}
      onClose={() => props.onClose()}
    >
      <Box sx={boxStyle}>
        {notice}
        {player1}
        {player2}
        <Autocomplete
          disablePortal
          options={autoCompleteOptions}
          renderInput={(params) => <TextField {...params} />}
          defaultValue={defaultMatchWinner}
          onChange={(_, newValue: string | null) => setMatchWinner(newValue ?? undefined)}
        />
        <Button onClick={() => onUpdate()}>Updaterino</Button>
        <Button onClick={() => autoBye()}>Auto Bye</Button>
      </Box>
    </StyledModal>
  );
}
