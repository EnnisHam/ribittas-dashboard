import { Accordion, AccordionSummary, AccordionDetails } from "@mui/material"; 
import { ExpandMore } from '@mui/icons-material'
import { Group, Match, Participant, Round } from "brackets-model";
import { IRoundComposition, RoundElement, composeRoundDetails } from "./round";
import { useMemo } from "react";
import { TournamentType, createGroupLabel } from "../../utils/groupNaming";
import { IMatchComposition } from "./match";

/**
 * Includes list of rounds within a group to display as a list
 * in navigation bar
 */
interface IGroupComposition extends Group {
  rounds: IRoundComposition[];
}

export const composeGroupDetails = (
  group: Group,
  rounds: Round[],
  matches: Match[],
  participants: Participant[],
): IGroupComposition => {
  const bracketRounds = rounds.filter((round) => round.group_id === group.id);
  const groupRounds = bracketRounds.map((round) => composeRoundDetails(round, matches, participants));

  return {
    ...group,
    rounds: groupRounds
  }
}

interface IGroupProps extends IGroupComposition {
  tournamentFormat: TournamentType | undefined;
  openModal: () => void;
  setMatchDetails: (details: IMatchComposition) => void;
  style?: React.CSSProperties;
}

export const GroupElement = (props: IGroupProps) => {
  const {
    rounds,
    tournamentFormat,
    openModal,
    setMatchDetails
  } = props;
  const groupId = Number(props.id);

  const roundElements = useMemo(() => rounds.map(
    (round, index) =>
      <RoundElement
        key={`group-${groupId}-round-${round.id}`}
        roundIndex={index + 1}
        openModal={openModal}
        setMatchDetails={setMatchDetails}
        {...round}
      />),
    [rounds, groupId]);
  const groupLabel = useMemo(() => tournamentFormat ? createGroupLabel(groupId, tournamentFormat) : `Group ${groupId}`, [groupId, tournamentFormat]);

  return (
    <Accordion>
      <AccordionSummary
        id={`group-${groupId}`}
        expandIcon={<ExpandMore />}
      >
        <span>{groupLabel}</span>
      </AccordionSummary>
      <AccordionDetails >
        {...roundElements}
      </AccordionDetails>
    </Accordion>
  );
}
