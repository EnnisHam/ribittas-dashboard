import {
  Match,
  Participant
} from 'brackets-model';
import { useCallback, useMemo } from 'react';
import Button from '@mui/material/Button';

/**
 * Includes participant information in the match
 */
export interface IMatchComposition extends Match {
  participants: [Participant | null, Participant | null]
}

export const composeMatchDetails = (
  match: Match,
  participants: Participant[]
): IMatchComposition => {
  const player1 = participants.find((player) => player.id === match.opponent1?.id);
  const player2 = participants.find((player) => player.id === match.opponent2?.id);

  return {
    ...match,
    participants: [player1 ?? null, player2 ?? null]
  }
}

export interface IMatchProps extends IMatchComposition {
  openModal: () => void;
  setMatchDetails: (details: IMatchComposition) => void;
  style?: React.CSSProperties;
}

export const MatchElement = (props: IMatchProps) => {
  const {
    participants,
    openModal,
    setMatchDetails
  } = props;
  const player1 = useMemo(() => participants[0]?.name ?? 'Awaiting Player', [participants]);
  const player2 = useMemo(() => participants[1]?.name ?? 'Awaiting Player', [participants]);

  const handleClick = useCallback(() => {
    setMatchDetails(props);
    openModal();
  }, [openModal, setMatchDetails]);

  return (
    <div>
      <Button onClick={handleClick}>{`${player1} vs ${player2}`}</Button>
    </div>
  );
}