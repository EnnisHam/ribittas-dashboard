import { Accordion, AccordionSummary, AccordionDetails } from "@mui/material"; 
import { ExpandMore } from '@mui/icons-material'
import { Match, Participant, Round } from 'brackets-model';
import { useMemo } from 'react';
import { MatchElement, IMatchComposition, composeMatchDetails } from './match';

/**
 * Includes list of matches within a round to display as a list
 * in navigation bar
 */
export interface IRoundComposition extends Round {
  matches: IMatchComposition[];
}

export const composeRoundDetails = (
  round: Round,
  matches: Match[],
  participants: Participant[]
): IRoundComposition => {
  const roundMatches = matches.filter((match) => match.round_id === round.id);
  const matchList = roundMatches.map((match) => composeMatchDetails(match, participants));

  return {
    ...round,
    matches: matchList
  }
}

export interface IRoundProps extends IRoundComposition {
  roundIndex?: number;
  openModal: () => void;
  setMatchDetails: (details: IMatchComposition) => void;
  style?: React.CSSProperties;
}

export const RoundElement = (props: IRoundProps) => {
  const {
    matches,
    roundIndex,
    openModal,
    setMatchDetails
  } = props;
  const roundId = props.id;

  const matchElements = useMemo(() =>
    matches.map((match) =>
      <MatchElement
        key={`round-${roundId}-match-${match.id}`}
        openModal={openModal}
        setMatchDetails={setMatchDetails}
        {...match}
      />
    ),
  [matches, roundId]);

  return (
    <Accordion>
      <AccordionSummary
        id={`round-${roundId}`}
        expandIcon={<ExpandMore />}
      >
        <span>{`Round ${roundIndex ?? roundId}`}</span>
      </AccordionSummary>
      <AccordionDetails>
        {...matchElements}
      </AccordionDetails>
    </Accordion>
  );
}