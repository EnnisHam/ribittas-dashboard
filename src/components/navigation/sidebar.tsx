import { Match, Participant, Round, Group } from 'brackets-model';
import React, { useMemo } from 'react';
import { GroupElement, composeGroupDetails } from './group';
import { numberToTournamentType } from '../../utils/groupNaming';
import { IMatchComposition } from './match';

type NavigationProps = {
  groupInfo: Group[];
  roundInfo: Round[];
  matchInfo: Match[];
  participantInfo: Participant[];
  openModal: () => void;
  setMatchDetails: (details: IMatchComposition) => void;
  style?: React.CSSProperties;
  className?: string
}

export const SideNavigation = (props: NavigationProps) => {
  const {
    groupInfo,
    roundInfo,
    matchInfo,
    participantInfo,
    openModal,
    setMatchDetails
  } = props;

  const tournamentFormat = useMemo(() =>
    numberToTournamentType(groupInfo.length),
  [groupInfo]);

  const groupList = useMemo(() => {
    const groups = groupInfo.map(
      (group) => composeGroupDetails(
        group, roundInfo, matchInfo, participantInfo
      )
    );

    const groupElements = groups.map(
      (group) =>
      <GroupElement
        tournamentFormat={tournamentFormat}
        openModal={openModal}
        setMatchDetails={setMatchDetails}
        { ...group }
      />
    );
    return groupElements;
  }, [groupInfo, roundInfo, matchInfo, participantInfo ]);


  return (
    <div id='navigation-container' style={props.style} className={props.className}>
      {...groupList}
    </div>
  );
}
